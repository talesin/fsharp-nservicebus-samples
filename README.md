NServiceBus Code First
----------------------
Following guide from the [step-by-step guide](http://docs.particular.net/nservicebus/nservicebus-step-by-step-guide), but using F#

If Raven doesn't install properly, follow the instructions in [Managing NServiceBus Using PowerShell](http://docs.particular.net/nservicebus/managing-nservicebus-using-powershell)

