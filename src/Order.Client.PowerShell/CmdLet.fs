﻿namespace Order.Client.PowerShell

open System
open System.ComponentModel
open System.Diagnostics
open System.Management.Automation
open NServiceBus
open Ordering.Messages

module Bus =
    let private bus = lazy (
        Configure.Serialization.Xml() |> ignore
        Configure.Transactions.Enable() |> ignore

        Configure
            .With()
            .DefaultBuilder()
            .MsmqSubscriptionStorage()
            .UseTransport<Msmq>()
            .PurgeOnStartup(false)
            .UnicastBus()
            .CreateBus()
            .Start(fun () -> Configure.Instance.ForInstallationOn<Installation.Environments.Windows>().Install()))

    let send (dest:string) (message:'a) =
        bus.Value.Send (dest, message) |> ignore
        ()

    let shutdown () =
        (bus .Value :?> IStartableBus).Dispose ()

[<Cmdlet("Send", "Order")>]
type SendOrderCommand() =
    inherit Cmdlet()
            
    [<Parameter(Position = 0, Mandatory = true)>]
    member val Product = String.Empty with get, set

    override this.ProcessRecord () =
        Bus.send "Ordering.Server" (new PlaceOrder(Product = this.Product))
        ()

[<Cmdlet("Shutdown", "Bus")>]
type ShutdownBusCommand() =
    inherit Cmdlet()

    override this.ProcessRecord () =
        Bus.shutdown ()
        ()

