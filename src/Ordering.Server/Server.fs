﻿namespace Ordering.Server

open System
open NServiceBus
open Ordering.Messages

type EndpointConfig () = 
    interface IConfigureThisEndpoint
    interface AsA_Server

type PlaceOrderHandler () = 
    member val Bus:IBus = null with get, set

    interface IHandleMessages<PlaceOrder> with
        member this.Handle (message) =
            printfn "Order for Product:%s placed with id: %A" message.Product message.Id