﻿namespace Ordering.Client

open System
open NServiceBus
open Ordering.Messages

type EndpointConfig () = 
    interface IConfigureThisEndpoint
    interface AsA_Server

type SendOrder () =
    member val Bus:IBus = null with get, set

    interface IWantToRunWhenBusStartsAndStops with
        member this.Start () =
            let rec placeOrders () =
                let order = new PlaceOrder(Product = "New Shoes")
                this.Bus.Send ("Ordering.Server", order) |> ignore
                printfn "=========================================================================="
                printfn "Send a new PlaceOrder message with id: %A" order.Id
                
                if (Console.ReadLine () <> null) then
                    placeOrders ()
                ()

            printfn "Press 'Enter' to send a message.To exit, Ctrl + C"
            placeOrders ()
            ()

        member this.Stop () =
            ()


