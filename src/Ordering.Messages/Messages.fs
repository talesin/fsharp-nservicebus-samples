﻿namespace Ordering.Messages

open System
open NServiceBus

type PlaceOrder () = 
    interface ICommand
    member val Id = Guid.NewGuid() with get, set
    member val Product = String.Empty with get, set
